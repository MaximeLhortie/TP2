var div = document.getElementById('result');
var searchTextEntry;
function search(){
    var xmlHttp = new XMLHttpRequest();
    var jsonResult;
    xmlHttp.onreadystatechange=function(){
        if (xmlHttp.readyState==4 && xmlHttp.status==200){
            jsonResult = JSON.parse(xmlHttp.responseText);
            div.innerHTML = "<br><b><u>Votre recherche :</u> </b>" +jsonResult[0];
            for (i = 1; i < jsonResult[1].length; i++){
                div.innerHTML += "<br><br>" + "<b><u>Résultat :</u> </b>" + jsonResult[1][i];   
                div.innerHTML += "<br>" + "<b><u>Déscription :</u> </b>" + jsonResult[2][i];                               
                div.innerHTML += "<br>" + "<b><u>Lien :</u> </b><a href="+jsonResult[3][i]+">" + jsonResult[3][i] + "</a>";
            }
        }
        console.log(xmlHttp.responseText);
    }
    xmlHttp.open("GET", 
                "http://localhost:1234/api/?action=opensearch&format=json&search=" + 
                document.getElementById('searchEntry').value +
                "&debug=true",
                true);
    xmlHttp.send();
}